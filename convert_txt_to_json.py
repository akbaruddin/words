import json

wordsList = []
wordsDict = {}

with open('words.json', 'w') as jsonfile:
    print("========Open JSON File==========\n")
    with open('words.txt', 'r') as words:
        print("========Open txt File===========\n")
        line = words.readline().replace("\n", "")
        while line != '':
            firstCharactor = line[0].lower()
            found = firstCharactor in wordsList
            if not found:
                wordsList.append(firstCharactor)
                wordsDict[firstCharactor] = []
            wordsDict[firstCharactor].append(line)
            line = words.readline().replace("\n", "")
    print("========Writing in file=========\n")
    jsonfile.write(json.dumps(wordsDict, indent = 4, sort_keys=True))

print("========Process Done============")