import yaml

wordsList = []
wordsDict = {}

with open('words.yaml', 'w') as jsonfile:
    print("========Open YAML File==========\n")
    with open('words.txt', 'r') as words:
        print("========Open txt File===========\n")
        line = words.readline().replace("\n", "")
        while line != '':
            firstCharactor = line[0].lower()
            found = firstCharactor in wordsList
            if not found:
                wordsList.append(firstCharactor)
                wordsDict[firstCharactor] = []
            wordsDict[firstCharactor].append(line)
            line = words.readline().replace("\n", "")
    print("========Writing in file=========\n")
    jsonfile.write(yaml.dump(wordsDict, explicit_start=True))

print("========Process Done============")